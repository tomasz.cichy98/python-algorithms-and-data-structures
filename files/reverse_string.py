"""
Python Data Structures - A Game-Based Approach
Stack challenge
Robin Andrews - https://compucademy.net/
"""

from stack import Stack

string = "gninraeL nIdekniL htiw tol a nraeL"
reversed_string = ""
s = Stack()
# Your solution here.
for l in string:
    s.push(l)

while not s.is_empty():
    reversed_string += s.pop()

print(reversed_string)
