# Python Algorithms and Data Structures

Python files for the [Python Data Structures and Algorithms](https://www.linkedin.com/learning/python-data-structures-and-algorithms/python-data-structures-and-algorithms-in-action) course in LinkedIn Learning.

## Contents
1. Stack
1. Depth-first search
1. Queue
1. Breath-first search
1. Priority Queue
1. A* algorithm
